# composer.json

## Symfony demo

### Symfony 4.4 based
 
```json
{
    "name": "symfony/symfony-demo",
    "license": "MIT",
    "type": "project",
    "description": "Symfony Demo Application",
    "require": {
        "php": "^7.1.3",
        "ext-pdo_sqlite": "*",
        "doctrine/doctrine-bundle": "^1.6.10",
        "doctrine/doctrine-migrations-bundle": "^1.3",
        "doctrine/orm": "^2.5.11",
        "erusev/parsedown": "^1.6",
        "ezyang/htmlpurifier": "^4.9",
        "sensio/framework-extra-bundle": "^5.0",
        "sensiolabs/security-checker": "^6.0",
        "symfony/asset": "^4.4",
        "symfony/expression-language": "^4.4",
        "symfony/flex": "^1.1",
        "symfony/form": "^4.4",
        "symfony/framework-bundle": "^4.4",
        "symfony/monolog-bundle": "^3.1",
        "symfony/polyfill-php72": "^1.8",
        "symfony/security-bundle": "^4.4",
        "symfony/swiftmailer-bundle": "^3.1",
        "symfony/translation": "^4.4",
        "symfony/twig-bundle": "^4.4",
        "symfony/validator": "^4.4",
        "symfony/yaml": "^4.4",
        "twig/extensions": "^1.5",
        "white-october/pagerfanta-bundle": "^1.1"
    },
    "require-dev": {
        "dama/doctrine-test-bundle": "^5.0",
        "doctrine/doctrine-fixtures-bundle": "^3.0",
        "friendsofphp/php-cs-fixer": "^2.12",
        "symfony/browser-kit": "^4.4",
        "symfony/css-selector": "^4.4",
        "symfony/debug-bundle": "^4.4",
        "symfony/dotenv": "^4.4",
        "symfony/phpunit-bridge": "^4.4",
        "symfony/stopwatch": "^4.4",
        "symfony/web-profiler-bundle": "^4.4",
        "symfony/web-server-bundle": "^4.4"
    },
    "config": {
        "platform": {
            "php": "7.1.3"
        },
        "preferred-install": {
            "*": "dist"
        },
        "sort-packages": true
    },
    "autoload": {
        "psr-4": {
            "App\\": "src/"
        }
    },
    "autoload-dev": {
        "psr-4": {
            "App\\Tests\\": "tests/"
        }
    },
    "scripts": {
        "auto-scripts": {
            "cache:clear": "symfony-cmd",
            "assets:install --symlink --relative %PUBLIC_DIR%": "symfony-cmd",
            "security-checker security:check": "script"
        },
        "post-install-cmd": [
            "@auto-scripts"
        ],
        "post-update-cmd": [
            "@auto-scripts"
        ]
    },
    "conflict": {
        "symfony/symfony": "*"
    },
    "extra": {
        "symfony": {
            "allow-contrib": true,
            "require": "4.4.*"
        }
    }
}
```

### Symfony 5.x based
 
```json
{
    "name": "symfony/symfony-demo",
    "license": "MIT",
    "type": "project",
    "description": "Symfony Demo Application",
    "minimum-stability": "stable",
    "prefer-stable": true,
    "replace": {
        "symfony/polyfill-php70": "*",
        "symfony/polyfill-php72": "*"
    },
    "require": {
        "php": "^7.2.9",
        "ext-pdo_sqlite": "*",
        "composer/package-versions-deprecated": "^1.8",
        "doctrine/doctrine-bundle": "^1.12|^2.0",
        "doctrine/doctrine-migrations-bundle": "^1.3|^2.0",
        "doctrine/orm": "^2.5.11",
        "erusev/parsedown": "^1.6",
        "sensio/framework-extra-bundle": "^5.1",
        "symfony/apache-pack": "^1.0",
        "symfony/asset": "^5.1",
        "symfony/console": "^5.1",
        "symfony/dotenv": "^5.1",
        "symfony/expression-language": "^5.1",
        "symfony/flex": "^1.1",
        "symfony/form": "^5.1",
        "symfony/framework-bundle": "^5.1",
        "symfony/intl": "^5.1",
        "symfony/mailer": "^5.1",
        "symfony/monolog-bundle": "^3.1",
        "symfony/polyfill-intl-messageformatter": "^1.12",
        "symfony/security-bundle": "^5.1",
        "symfony/string": "^5.1",
        "symfony/translation": "^5.1",
        "symfony/twig-pack": "^1.0",
        "symfony/validator": "^5.1",
        "symfony/webpack-encore-bundle": "^1.4",
        "symfony/yaml": "^5.1",
        "tgalopin/html-sanitizer-bundle": "^1.2",
        "twig/intl-extra": "^3.0",
        "twig/markdown-extra": "^3.0"
    },
    "require-dev": {
        "dama/doctrine-test-bundle": "^6.2",
        "doctrine/doctrine-fixtures-bundle": "^3.0",
        "friendsofphp/php-cs-fixer": "3.0.x-dev",
        "symfony/browser-kit": "^5.1",
        "symfony/css-selector": "^5.1",
        "symfony/debug-bundle": "^5.1",
        "symfony/maker-bundle": "^1.11",
        "symfony/phpunit-bridge": "^5.1",
        "symfony/stopwatch": "^5.1",
        "symfony/web-profiler-bundle": "^5.1"
    },
    "config": {
        "platform": {
            "php": "7.2.9"
        },
        "preferred-install": {
            "*": "dist"
        },
        "sort-packages": true
    },
    "autoload": {
        "psr-4": {
            "App\\": "src/"
        }
    },
    "autoload-dev": {
        "psr-4": {
            "App\\Tests\\": "tests/"
        }
    },
    "scripts": {
        "auto-scripts": {
            "cache:clear": "symfony-cmd",
            "assets:install --symlink --relative %PUBLIC_DIR%": "symfony-cmd"
        },
        "post-install-cmd": [
            "@auto-scripts"
        ],
        "post-update-cmd": [
            "@auto-scripts"
        ]
    },
    "conflict": {
        "symfony/symfony": "*"
    },
    "extra": {
        "symfony": {
            "allow-contrib": true,
            "require": "5.1.*"
        }
    }
}
```
